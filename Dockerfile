FROM openjdk:8-jdk
MAINTAINER Jean-Michel F <cv@jeanmichelfahys.net>

ENV ANDROID_TARGET_SDK="24" \
    ANDROID_BUILD_TOOLS="24.0.1" \
    ANDROID_SDK_TOOLS="24.4.1"

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

RUN wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/android-sdk_r${ANDROID_SDK_TOOLS}-linux.tgz && \
    tar --extract --gzip --file=android-sdk.tgz

RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter android-${ANDROID_TARGET_SDK} && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter platform-tools && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS}

RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-google_play_services && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository

# Android 5.0 Virtual Device Image - can use android instead of google_apis
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter sys-img-x86-google_apis-${ANDROID_TARGET_SDK} 

ENV ANDROID_HOME $PWD/android-sdk-linux

# Create an Android 5.0 Virtual Device - can remove google_aîs here
RUN echo "no" | android-sdk-linux/tools/android create avd --force -n testAVD -t android-${ANDROID_TARGET_SDK} --abi google_apis/x86 

# Let's get sonar-lint
RUN wget --quiet --output-document=sonarlint.zip https://bintray.com/sonarsource/Distribution/download_file?file_path=sonarlint-cli%2Fsonarlint-cli-2.0.zip && \
    unzip -q sonarlint.zip && rm sonarlint.zip && chmod +x $PWD/sonarlint-cli-2.0/bin/sonarlint
    
ENV SONAR_EXEC_HOME $PWD/sonarlint-cli-2.0/bin
