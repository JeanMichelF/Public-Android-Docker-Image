#!/bin/bash

# This script generate an index.html files based ont the html files in subdirectories
# Perfect for Gitlab Pages based on build artifacts

ROOT=$1
OUTPUT=$ROOT"/index.html"
ROOT_LENGHT=${#ROOT}

# loop & print a folder recusively,
print_folder_recurse() {
    # Only if interesting HTML file exists
    count=`find $1 \( -name "index.html" -o -name "*lint*.html" \) 2>/dev/null| wc -l`
    if [ $count != 0 ]
    then 
        echo "<UL>" >> $OUTPUT
        for i in "$1"/*;do
            if [ -d "$i" ];then
                #echo "dir: $i"
                file=`basename "$i"`
                echo "    <LI>$file</LI>" >> $OUTPUT
                print_folder_recurse "$i" $2
            elif [ -f "$i" ]; then
                #echo "file: $i"
                file=`basename "$i"`
                # Stripping the first caracter and adding a point => .public become ./
                echo "    <LI><a href=\"./${i:$2}\">$file</a></LI>" >> $OUTPUT
            fi
        done
        echo "</UL>" >> $OUTPUT
    fi
}

#echo "base path: $path"
echo "Beginning of makeIndexHtml.sh"
echo "" > $OUTPUT
print_folder_recurse $ROOT $(($ROOT_LENGHT+1))
echo "End of makeIndexHtml.sh"